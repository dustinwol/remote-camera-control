# Introduction
This project is used to enable a remote control for cameras, whose are controllable by the microsoft DirectShow API. Mainly it was tested & developed for the Logitech BCC950 ConferenceCam. 
It consists of two parts:
1. Host: C# Application with integrated CefSharp-browser to serve as Server with connected camera.
2. Client: simple one-file HTML application

# Connection
Under the hood this application uses the WebRTC protocol implemented by [PeerJS](https://peerjs.com/) to establish a peer-to-peer connection. To establish a connection, it uses a public accessible PeerJsServer.
To protect from unwarranted peer connections, the connection is secured with symmetric AES-GCM encryption additional to the [WebRTC encryption](https://webrtc-security.github.io/).
The host is responsible for sending password & connection-ID to the client out of the PeerJS network.

# How-To
## Host-side
1. Connect camera to host 
2. Start **RemoteControlWebcam.exe** on host
3.  (opt) Enter name of camera if it doesn't get detected automatically
4. Check *started* checkbox to start internal browser
5.  (opt) Enter address of your PeerJS server
6. Send *password* & *connection ID* to client (user has to determine own communication way)

## Client-side
1. Open **Client.html** on client
2. Enter *PeerJS Server*, *password* & *connection ID*
3. Click *start connection*

You're ready to use!

# Known issues
After some time, both, the host and the client may looses the connection to the signaling server. While established connections between host and clients will remain, the host needs to reconnect to the signaling server to accept new clients, which is done automatically. 
There is no need for the client to reconnect until it needs to connect to another host, which is done automatically too.

# Appendix
## signaling server
While it is possible to use the public PeerJS server, it is strongly recommended to set up an own [PeerJS server](https://github.com/peers/peerjs-server) to use it as signaling server.
The public PeerJS Server should be used only for testing

## Client application
The client was designed as single file application which can easily be delivered. Because of that, it would be possible to host the file on some webserver, so the user then just needs to know the credentials to access to the webcam.