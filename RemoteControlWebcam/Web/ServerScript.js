let key = 'default'; //plain Key used for decryption
let peerJsServer = ''
let connectedPeers = new Map();
let connectionID = '';
let mediaStreamOpen = null;
let mediaStreamEnabled = true;

function setConfiguration(password, serverAddress, enableMediaStream) {
	key = password;
	peerJsServer = serverAddress;
	setMediaStreamEnabled(enableMediaStream);
}

function setMediaStreamEnabled(value) {
	mediaStreamEnabled = Boolean(value);
	console.log('mediaStreamEnabled set to: ' + mediaStreamEnabled);
}

// watchdog for media streams. disconnects stream if Client doesn't replied since > 5s.
let watchdogRunning = false;

async function startWatchdog() {
	if (!watchdogRunning) {
		watchdogLoop();
	}
	watchdogrunning = true;
}

// check connected peers for lost connections
function watchdogLoop() {
	setTimeout(() => {
		if (connectedPeers.size == 0) {
			return;
		}
		let currentTime = Date.now();
		let streamsOpen = 0;

		for (let [key, value] of connectedPeers.entries()) {
			// if last ACK-message was send >5s ago, remove connection to client
			if (value.mediaStream) {
				streamsOpen++;
				if (!mediaStreamEnabled) {
					streamsOpen--;
					value.mediaStream.close();
				}
			}

			if ((currentTime - value.lastConnect) > 5000) {
				if (value.mediaStream) {
					streamsOpen--;
					value.mediaStream.close();
				}
				value.connection.close();

				console.log('Connection closed to peer ' + key);
				connectedPeers.delete(key);
			}
		}

		// close mediaStream if no peer left to receive
		if (streamsOpen == 0) {
			mediaStreamOpen.getTracks().forEach(function (track) {
				track.stop();
			});
		}

		watchdogLoop();
	}, 3000);
}

function reconnect(peer) {
	if (peer.destroyed) {
		console.log('Destroyed: ' + peer.destroyed);
		openPeer();
		return;
	}
	peer.reconnect();
	console.log('Reconnecting.' + peer.destroyed );

	setTimeout(() => {
		if (peer.disconnected) {
			JsBounderAsync.displayMessage('Failed to reconnect. Try again in 5s.' + peer.destroyed + Date.now().toLocaleString());

			setTimeout(() => {
				reconnect(peer);
			}, 5000);
		} else {
			JsBounderAsync.displayMessage('reconnected to signaling server');
		}
	}, 1000);
}

function openPeer() {
	let peer;

	if (peerJsServer == '') {
		if (connectionID == '') {
			peer = new Peer();
		} else {
			peer = new Peer(connectionID);
		}
	} else {
		if (connectionID == '') {
			peer = new Peer({ host: peerJsServer });
		} else {
			peer = new Peer(connectionID, { host: peerJsServer });
		}
	}

	peer.on('error', err => {
		JsBounderAsync.onError('Peer: ' + err.toString());
	});

	peer.on('disconnected', () => {
		//JsBounderAsync.displayMessage('Disconnected from signaling Server. Reconnecting.' + peer.destroyed);
		reconnect(peer);
	});

	peer.on('open', id => {
		// Server is open for peer connections now
		JsBounderAsync.displayMessage('Connected with ID: ' + peer.id);
		JsBounderAsync.onConnectionOpened(id);
		connectionID = peer.id;

		peer.on('connection', connection => {
			// Peer connected to Server
			let authenticated = false;  // indicated wether the peer has authenticated himself with correct password

			connectedPeers.set(connection.peer, {
				connection: connection,
				mediaStream: null,
				lastConnect: Date.now()
			});

			startWatchdog();

			connection.on('error', err => {
				JsBounderAsync.onError('Con: ' + err.toString());
			});

			connection.on('close', () => {
				JsBounderAsync.displayMessage('Connection closed to Peer ' + connection.peer);
			})

			connection.on('data', data => {
				/** Following instruction are interpreted by the server if encrypted with correct password:
				 * Request:	Establish an mediaStream connection to the client (and authenticate peer)
				 * SYN: authenticate peer to send instructions
				 * ACK: im-alive acknowledgement for identifiying lost connection (because firefox don't cause the close events)
				 * setIntensity [value]: set the intensity to allow greater camera movement
				 * 
				 * Other instructions are interpreted as camera instruction, if the peer was successful authenticated.
				 * **/

				// Data received from peer
				if (key == 'default') {
					JsBounderAsync.displayMessage('Key was not set! decryption of input data might fail!');
				}

				// decrypt data
				aesGcmDecrypt(data, key).then(plaintext => {
					// TODO: replace string identification with type identification
					// check for keyword or send received data to host-program
					if (plaintext == 'Request') {
						// Peer successful authenticated with correct Password
						authenticated = true;
						connection.send('ACK');
						JsBounderAsync.onPeerConnected(connection.peer);

						if (!mediaStreamEnabled) {
							return;
						}

						// send Mediastream to Peer
						navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then(stream => {
							let mediaStream = peer.call(connection.peer, stream);
							mediaStreamOpen = stream;

							let myConnectionObj = connectedPeers.get(connection.peer);
							myConnectionObj.mediaStream = mediaStream;
							myConnectionObj.lastConnect = Date.now();
							connectedPeers.set(connection.peer, myConnectionObj);

							mediaStream.on('error', err => {
								JsBounderAsync.onError(err.toString());
							});

							// does not get called in case of Firefox Browser at one end
							mediaStream.on('close', () => {
								JsBounderAsync.displayMessage('Mediastream closed to Peer ' + mediaStream.peer);
							});

						}).catch(err => {
							JsBounderAsync.onError('MediaStream: ' + err.toString());
						});

					} else if (plaintext == 'ACK') {
						// check for ACK msg from peer (because firefox don't cause close event)
						let myConnectionObj = connectedPeers.get(connection.peer);
						myConnectionObj.lastConnect = Date.now();
						connectedPeers.set(connection.peer, myConnectionObj);

					} else if (plaintext == 'SYN') {
						// client wants to check connection
						connection.send('ACK');
						authenticated = true;
						JsBounderAsync.onPeerConnected(connection.peer);

					} else if (plaintext.startsWith('setIntensity ')) {
						let intensity = plaintext.substr(13);
						JsBounderAsync.displayMessage('Intensity: ' + intensity);
						if (intensity != null) {
							JsBounderAsync.onSetIntensity(connection.peer, parseInt(intensity));
						}

					} else if (authenticated) {
						// determine task to execute
						JsBounderAsync.onDataReceived(connection.peer, plaintext);
					}
				}).catch(err => {
					JsBounderAsync.onError('failed to encrypt data from peer: ' + err.toString());
				});
			});
		});
	});
}

// bind JS to C# Object
(async function () {
	await CefSharp.BindObjectAsync("JsBounderAsync", "bound");
})();

document.onclose = () => {
	peer.close();
};