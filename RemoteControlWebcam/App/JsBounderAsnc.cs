﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlWebcam
{
    // This class is used to get bound to JavaScript by CefSharp browser
    // it executes JS calls to C# methods asynchronous
    public class JsBounderAsnc
    {
        private readonly MainForm gui;
        private Dictionary<string, int> intensitys = new Dictionary<string, int>();

        public JsBounderAsnc(MainForm gui)
        {
            this.gui = gui;
        }

        public void DisplayMessage(string message)
        {
            this.gui.ShowMessage(message, MessageType.INFORMATION);
        }

        public void OnSetIntensity(string peer, int intensity)
        {
            if (!this.intensitys.Keys.Contains(peer))
            {
                this.gui.ShowMessage("Unknown peer " + peer + "tried to set intensity!", MessageType.INFORMATION);
                return;
            }

            this.intensitys[peer] = intensity;
            this.gui.ShowMessage("Intensity of " + peer + " set to " + intensity, MessageType.INFORMATION);
        }

        public void OnError(object error)
        {
            Console.WriteLine(error.ToString());
            this.gui.ShowMessage("Error:" + error.ToString(), MessageType.INFORMATION);
        }

        public void OnConnectionOpened(string peer)
        {
            this.gui.ShowMessage(peer, MessageType.CONNECTION_ID);
        }

        public void OnDataReceived(string peer, string data)
        {
            this.gui.ShowMessage("Task received:" + data, MessageType.INFORMATION);

            // set intensity
            int intensity = 1;
            if(this.intensitys.ContainsKey(peer))
            {
                intensity = this.intensitys[peer];
            }

            CameraControl cameraControl = CameraControlFactory.Get();
            cameraControl.Intensity = intensity;

            switch (data)
            {
                case "Up":
                    cameraControl.Up();
                    break;                
                case "Down":
                    cameraControl.Down();
                    break;               
                case "Left":
                    cameraControl.Left();
                    break;               
                case "Right":
                    cameraControl.Right();
                    break;
                case "ZoomIn":
                    cameraControl.ZoomIn();
                    break;
                case "ZoomOut":
                    cameraControl.ZoomOut();
                    break;
                default:
                    this.gui.ShowMessage("Unknown Task received:", MessageType.INFORMATION);
                    break;
            }
        }

        public void OnPeerConnected(string peer)
        {
            gui.ShowMessage("Peer with ID:" + peer + " connected.", MessageType.INFORMATION);
            this.intensitys.Add(peer, 1);
        }

        public void OnPeerDisconnected(string peer)
        {
            gui.ShowMessage("Peer disconnected: " + peer, MessageType.INFORMATION);
            this.intensitys.Remove(peer);
        }
    }
}
