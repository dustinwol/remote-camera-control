﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Configuration;

using CefSharp;
using CefSharp.OffScreen;

namespace RemoteControlWebcam
{
    public partial class MainForm : Form
    {
        private readonly CameraControl cameraControl;
        private ChromiumWebBrowser browser;
        private static readonly string peerJsServerConfig = "peerJsServer";

        public MainForm()
        {
            InitializeComponent();

            // create Camera control
            cameraControl = CameraControlFactory.Get();
        }

        private ChromiumWebBrowser InitializeCefSharp()
        {
            // enable media stream
            if( !Cef.IsInitialized)
            {
                CefSettings cefSettings = new CefSettings();
                cefSettings.CefCommandLineArgs.Add("enable-media-stream", "1");
                Cef.Initialize(cefSettings);
            }

            // create browser
            browser = new ChromiumWebBrowser(GetUrl());

            // link JS to JsBounderAsync C# class
            browser.JavascriptObjectRepository.Register("JsBounderAsync", new JsBounderAsnc(this), true);

            // generate key & display it to user
            string key = KeyGenerator.GetUniqueKey(16);
            //key = "Password"; // just for debugging
            ShowMessage(key, MessageType.PASSWORD);

            browser.ExecuteScriptAsyncWhenPageLoaded("setConfiguration('" + key + "', '" + GetServerAddress() + "', " + Convert.ToInt32(checkBoxMediaStream.Checked) + ");", true);
            browser.ExecuteScriptAsyncWhenPageLoaded("openPeer();", true);

            return browser;
        }

        private string GetUrl()
        {
            return "file:\\\\" + Directory.GetCurrentDirectory() + "\\Web\\Server.html";
        }

        private string GetServerAddress()
        {
            Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            KeyValueConfigurationCollection settings = configFile.AppSettings.Settings;

            if(settings[peerJsServerConfig] == null)
            {
                return SetServerAddress();
            }

            return settings[peerJsServerConfig].Value;
        }

        private string SetServerAddress()
        {
            // retrieve server address from user
            InsertDialog insertServerAddress = new InsertDialog(InsertDialogType.PEERJS_SERVER_ADDRESS);
            insertServerAddress.ShowDialog();

            string serverAddress = "";
            if (!insertServerAddress.IsCheckBoxChecked)
            {
                serverAddress = insertServerAddress.GetTextEntry;
            }

            // save address in configuration file
            Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            KeyValueConfigurationCollection settings = configFile.AppSettings.Settings;

            if (settings[peerJsServerConfig] == null)
            {
                settings.Add(peerJsServerConfig, serverAddress);
            } else
            {
                settings[peerJsServerConfig].Value = serverAddress;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            return serverAddress;
        }

        public void ShowMessage(string message, MessageType type)
        {
            Label label;
            switch(type)
            {
                case MessageType.CONNECTION_ID:
                    label = labelIDValue;
                    break;
                case MessageType.PASSWORD:
                    label = labelPasswordValue;
                    break;
                default:
                    label = labelMessage;
                    break;
            }
            label.Invoke((Action)(() => {
                label.Text = message;
            }));
        }

        private void buttonLeft_Click(object sender, EventArgs e)
        {
            cameraControl.Left();
        }

        private void buttonRight_Click(object sender, EventArgs e)
        {
            cameraControl.Right();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            cameraControl.Up();
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            cameraControl.Down();
        }

        private void buttonZoomIn_Click(object sender, EventArgs e)
        {
            cameraControl.ZoomIn();
        }

        private void buttonZoomOut_Click(object sender, EventArgs e)
        {
            cameraControl.ZoomOut();
        }

        private void checkBoxStart_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxStart.Checked)
            {
                browser = InitializeCefSharp();
                ShowMessage("Host server started. This will take a moment.", MessageType.INFORMATION);
            } else {
                browser.Dispose();
                ShowMessage("-", MessageType.PASSWORD);
                ShowMessage("-", MessageType.CONNECTION_ID);
            }
        }

        private void checkBoxMediaStream_CheckedChanged(object sender, EventArgs e)
        {
            if(browser != null)
            {
                browser.ExecuteScriptAsync("setMediaStreamEnabled(" + Convert.ToInt32(checkBoxMediaStream.Checked) + ");");
            }
        }

        private void buttonConfigureServer_Click(object sender, EventArgs e)
        {
            SetServerAddress();
        }
    }
}
