﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlWebcam
{
    public enum MessageType: int
    {
        PASSWORD,
        CONNECTION_ID,
        INFORMATION
    }
}
