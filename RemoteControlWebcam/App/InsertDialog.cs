﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControlWebcam
{
    public enum InsertDialogType: int
    {
        CAMERA_DEVICE_NAME,
        PEERJS_SERVER_ADDRESS
    }

    public partial class InsertDialog : Form
    {
        public InsertDialog(InsertDialogType type)
        {
            InitializeComponent();

            switch(type)
            {
                case InsertDialogType.CAMERA_DEVICE_NAME:
                    this.Text = "Device name";
                    this.labelPrimary.Text = "The device name is incorrect or the device is disconnected. Please insert correct device name";
                    this.labelSecondary.Text = "Default: 'BCC950 ConferenceCam'";
                    this.textBox1.Text = "BCC950 ConferenceCam";
                    this.checkBox1.Visible = false;

                    break;
                case InsertDialogType.PEERJS_SERVER_ADDRESS:
                    this.Text = "PeerJS server";
                    this.labelPrimary.Text = "Please enter the server address to configure the server.";
                    this.labelSecondary.Text = "enable checkbox to use default PeerJS server (mainly used for testing)";
                    this.textBox1.Text = "";
                    this.checkBox1.Text = "Default";
                    this.checkBox1.Visible = true;

                    Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    KeyValueConfigurationCollection settings = configFile.AppSettings.Settings;

                    if (settings["peerJsServer"] != null)
                    {
                        this.textBox1.Text = settings["peerJsServer"].Value;
                    }

                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string GetTextEntry
        {
            get
            {
                return textBox1.Text;
            }
        }

        public bool IsCheckBoxChecked
        {
            get
            {
                return checkBox1.Checked;
            }
        }
    }
}
