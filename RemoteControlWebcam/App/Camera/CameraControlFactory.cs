﻿using System;
using PTZ;

namespace RemoteControlWebcam
{
    static class CameraControlFactory
    {
        public static CameraControl Get()
        {
            //return new CameraControl(null);
            return GetByName("BCC950 ConferenceCam");
        }   
        
        public static CameraControl GetByName(string name)
        {
            try
            {
                PTZDevice camera = PTZDevice.GetDevice(name, PTZType.Relative);
                return new CameraControl(camera);
            }
            catch (ApplicationException ex)
            {
                // camera not successfull detected. Check Device name
                System.Console.WriteLine(ex.Message);

                // open new Form to enter correct Device name
                InsertDialog insertDeviceName = new InsertDialog(InsertDialogType.CAMERA_DEVICE_NAME);
                insertDeviceName.ShowDialog();

                if (insertDeviceName.ActiveControl?.GetType()?.Name != "Button")
                {
                    System.Environment.Exit(0);
                }

                // recursive call in case of false device name
                return GetByName(insertDeviceName.GetTextEntry);
            }
        }
    }
}
