﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using PTZ;

namespace RemoteControlWebcam
{
    public class CameraControl
    {
        private readonly PTZDevice camera;
        private int intensity;
        private static readonly Mutex mutex = new Mutex();

        public int Intensity { 
            get
            {
                return intensity;
            }
            set
            {
                if( value < 6 && value > 0)
                {
                    intensity = value;
                }
            }
        }

        public CameraControl(PTZDevice device)
        {
            this.camera = device;
            this.intensity = 2;
        }

        private void ExecuteThreadSafe(Action handler)
        {
            mutex.WaitOne();
            try
            {
                // call handler multiple, because value of x/y is ignored by camera
                for(int i = 0; i < intensity; i++)
                {
                    handler();
                }
            } catch (Exception ex)
            {
                Console.WriteLine("CameraControl: failed to execute selected Action");
            } finally
            {
                mutex.ReleaseMutex();
            }
        }

        public void Up()
        {
            ExecuteThreadSafe(() => this.camera.Move(0, 1));
        }
        public void Down()
        {
            ExecuteThreadSafe(() => this.camera.Move(0, -1));
        }
        public void Left()
        {
            ExecuteThreadSafe(() => this.camera.Move(-1, 0));
        }
        public void Right()
        {
            ExecuteThreadSafe(() => this.camera.Move(1, 0));
        }

        public void ZoomIn()
        {
            ExecuteThreadSafe(() => this.camera.Zoom(1));
        }

        public void ZoomOut()
        {
            ExecuteThreadSafe(() => this.camera.Zoom(-1));
        }
    }
}
